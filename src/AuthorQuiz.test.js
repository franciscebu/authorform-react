import React from "react";
import ReactDOM from "react-dom";
import AuthorQuiz from "./AuthorQuiz";
import Enyzme, { mount, shallow, render } from "enzyme";
import Adapter from "enzyme-adapter-react-16";

Enyzme.configure({ adapter: new Adapter() });
describe("Author Quiz", () => {
  it("renders without crashing", () => {
    const div = document.createElement("div");
    ReactDOM.render(<AuthorQuiz />, div);
  });
});


